<?php
function mucu_accounts_settings($form, &$form_state) 
{
  $form = array();
  // only administrators can access this function

  // Generate the form - settings applying to all patterns first
  $form['ac'] = array(
    '#type' => 'fieldset',
    '#weight' => -30,
    '#title' => t('Main Account settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['ac']['mucu_main_account_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Main account name'),
    '#default_value' => variable_get('mucu_main_account_name', 'MAIN '),
    '#description' => t("Main account name"),
  );

  return system_settings_form($form);
}

