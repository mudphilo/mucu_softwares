<?php
/**
* Implements hook_schema().
*/
function mucu_accounts_schema() 
	{
		$schema['mucu_accounts_treasurer'] = array(
			'description' => 'Stores information about treasurers',
			'fields' => array(
					'auto' => array(
					'type' => 'serial',
					'not null' => TRUE,
					'description' => 'Primary Key:',
					),				
					'empno' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('Admission number.'),
					),
					'status' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'active',
					'description' => t('Current status of the treasurer'),
					),										
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),
									
			),
			'primary key' => array('auto'),
			
			);
			$schema['mucu_accounts_cashbook'] = array(
			'description' => 'Stores information about cash flow',
			'fields' => array(
					'auto' => array(
					'type' => 'serial',
					'not null' => TRUE,
					'description' => 'Primary Key:',
					),
					'period' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '13/14',
					'description' => t('Accounting period'),
					),										
					'account_id' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('account ID.'),
					),
					'item_id' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('Item ID.'),
					),
					'amount' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Amount'),
					),										
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),
									
			),
			'primary key' => array('auto'),
			
			);			
			$schema['mucu_accounts_transaction'] = array(
			'description' => 'Stores information about transactions',
			'fields' => array(
					'auto' => array(
					'type' => 'serial',
					'not null' => TRUE,
					'description' => 'Primary Key:',
					),
					'period' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '13/14',
					'description' => t('Accounting period'),
					),										
					'account_id' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('account ID.'),
					),
					'item_id' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('Item ID.'),
					),
					'amount' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Amount'),
					),
					'description' => array(
					'type' => 'varchar',
					'length' => 500, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Description'),
					),
					'transaction_date' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Transaction date'),
					),	
					'treasurer' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('treasurer who entered this record'),
					),
					'admittee' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('the person who admitted receiving/using the money'),
					),
					'folio' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Ledger folio'),
					),																												
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),
									
			),
			'primary key' => array('auto'),
			
			);			
			$schema['mucu_accounts_cash'] = array(
			'description' => 'Stores information about cash flow',
			'fields' => array(
					'auto' => array(
					'type' => 'serial',
					'not null' => TRUE,
					'description' => 'Primary Key:',
					),
					'period' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '13/14',
					'description' => t('Accounting period'),
					),										
					'account_id' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('account ID.'),
					),
					'cash' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('cash at hand.'),
					),
					'bank' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('cash at bank'),
					),					
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),
									
			),
			'primary key' => array('auto'),
			);	
					
			$schema['mucu_accounts_accounts'] = array(
			'description' => 'Stores information about account types',
			'fields' => array(
					'auto' => array(
					'type' => 'serial',
					'not null' => TRUE,
					'description' => 'Primary Key:',
					),
					'parent' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '1',
					'description' => t('Parent account.'),
					),
					'account_id' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => 'Account ID',
					),
					'account_name' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('account name.'),
					),					
					'type' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('Account type'),
					),
					'treasurer' => array(
					'type' =>'varchar',
					'length' =>200, 
					'not null' => TRUE,
					'default' => '1',
					'description' => t('account name.'),
					),	
					'status' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Current status,active,suspend'),
					),														
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),
									
			),
			'primary key' => array('auto'),
			
			);			

			$schema['mucu_accounts_type'] = array(
			'description' => 'Stores information about income and expenditure types',
			'fields' => array(
					'auto' => array(
					'type' => 'serial',
					'not null' => TRUE,
					'description' => 'Primary Key:',
					),
					'period' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '13/14',
					'description' => t('Accounting period'),
					),						
					'account_id' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('account ID.'),
					),
					'item_id' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' =>t('Item ID'),
					),
					'name' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Item name'),
					),	
					'type' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('Item type.'),
					),
					'budgeted' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('Budgeted amount'),
					),
					'real_amount' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Real amount'),
					),														
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),								
			),
			'primary key' => array('auto'),
			
			);	
			$schema['mucu_accounts_debtor'] = array(
			'description' => 'Stores information about debtors',
			'fields' => array(
					'auto' => array(
					'type' => 'serial',
					'not null' => TRUE,
					'description' => 'Primary Key:',
					),
					'period' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '13/14',
					'description' => t('Accounting period'),
					),						
					'account_id' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('account ID.'),
					),
					'empno' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' =>t('Admission number of the debtor'),
					),
					'amount' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Amount given'),
					),														
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),								
			),
			'primary key' => array('auto'),
			
			);		
			$schema['mucu_accounts_creditor'] = array(
			'description' => 'Stores information about creditors',
			'fields' => array(
					'auto' => array(
					'type' => 'serial',
					'not null' => TRUE,
					'description' => 'Primary Key:',
					),
					'period' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '13/14',
					'description' => t('Accounting period'),
					),						
					'account_id' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('account ID.'),
					),
					'empno' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' =>t('ID number of the debtor'),
					),
					'amount' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Amount given'),
					),														
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),								
			),
			'primary key' => array('auto'),
			
			);
			$schema['mucu_accounts_assets'] = array(
			'description' => 'Stores information about mucu assets',
			'fields' => array(
					'auto' => array(
					'type' => 'serial',
					'not null' => TRUE,
					'description' => 'Primary Key:',
					),
					'id' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '13/14',
					'description' => t('Unique asset ID'),
					),					
					'name' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '13/14',
					'description' => t('Asset name'),
					),
					'quantity' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '13/14',
					'description' => t('Number of items'),
					),						
					'date_purchased' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('Date this item was purchased.'),
					),
					'unit_price' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' =>t('Unit price of each items'),
					),
					'total' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Total amount'),
					),
					'valuation' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('valuation'),
					),														
					'depr_class' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Depreciation class'),
					),														
					'acc_depr_bf' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Total amount depraciated before this accounting period'),
					),														
					'depr_per_yr' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Depreciation per year'),
					),														
					'acc_depr_cd' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Total depreciation'),
					),														
					'net_book_value' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Net book value'),
					),														
					'remarks' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Remarks about the asset'),
					),																																							
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),								
			),
			'primary key' => array('auto'),
			
			);
			$schema['mucu_accounts_depreciation'] = array(
			'description' => 'Stores information about all depreciation class',
			'fields' => array(
					'auto' => array(
					'type' => 'serial',
					'not null' => TRUE,
					'description' => 'Primary Key:',
					),
					'name' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '13/14',
					'description' => t('Unique class name'),
					),						
					'method' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('Depreciation method .'),
					),
					'rate' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' =>t('Depreciation rate per year'),
					),
					'fixed_value' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Fixed value deducted per year'),
					),														
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),								
			),
			'primary key' => array('auto'),
			
			);		

			$schema['mucu_accounts_stock'] = array(
			'description' => 'Stores information about all salable goods/services used to raise money',
			'fields' => array(
					'auto' => array(
					'type' => 'serial',
					'not null' => TRUE,
					'description' => 'Primary Key:',
					),
					'account_id' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '1',
					'description' => t('Account which the stock belongs'),
					),						
					'name' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('Unique item name .'),
					),
					'cost' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' =>t('Total incurred cost in purchasing the item'),
					),
					'quantity' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Available quantity'),
					),			
					'sp' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Selling price per unit'),
					),																
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),								
			),
			'primary key' => array('auto'),
			
			);		
// index table
		$schema['mucu_accounts_index'] = array(
			'description' => 'Stores information notes to accounts books',
			'fields' => array(
					'auto' => array(
					'type' => 'serial',
					'not null' => TRUE,
					'description' => 'Primary Key:',
					),				
					'period' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '13/14',
					'description' => t('Accounting period'),
					),															
					'number' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '1',
					'description' => t('Document index number.'),
					),
					'name' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => '1',
					'description' => t('Document name.'),
					),

					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),
									
			),
			'primary key' => array('auto'),
			
			);
		$schema['mucu_accounts_year'] = array(
			'description' => 'Stores accounting period',
			'fields' => array(
					'auto' => array(
					'type' => 'serial',
					'not null' => TRUE,
					'description' => 'Primary Key:',
					),				
					'count' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('Period count.'),
					),
					'name' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'active',
					'description' => t('Accounting period name'),
					),
					'start_date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When the accounting period started.'),
					),
					'end_date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When the accounting period ends.'),
					),															
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),
									
			),
			'primary key' => array('auto'),
			
			);
		$schema['mucu_accounts_settings'] = array(
			'description' => 'Stores settings',
			'fields' => array(
					'auto' => array(
					'type' => 'serial',
					'not null' => TRUE,
					'description' => 'Primary Key:',
					),				
					'period' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('Accounting perios.'),
					),				
					'parameter' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu/1/1',
					'description' => t('Parameter.'),
					),
					'value' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'active',
					'description' => t('value'),
					),
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),
									
			),
			'primary key' => array('auto'),
			
			);

										
		return $schema;
	}
function mucu_accounts_uninstall() {
  // Drop my tables.
  drupal_uninstall_schema('mucu_accounts');
}
					