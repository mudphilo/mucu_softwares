<?php
/**
* Implements hook_schema().
*/
function mucu_library_schema() 
	{
		$schema['library_books'] = array(
			'description' => 'Stores information about books',
			'fields' => array(
					'auto' => array(
					'type' => 'serial',
					'not null' => TRUE,
					'description' => 'Primary Key:',
					),
					'name' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'not defined',
					'description' => t('Book name.'),
					),
					'author' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'MUCU',
					'description' => t('Author of the book.'),
					),
					'publisher' => array(
					'type' => 'varchar',
					'length' => 200, 
					'not null' => TRUE,
					'default' => 'mucu',
					'description' => t('Publisher of the book'),
					),
					'category' => array(
					'type' => 'varchar',
					'length' => 80, 
					'not null' => TRUE,
					'default' => 'lendable',
					'description' => t('Category of the book whish defines its lending rules or sales rules.'),
					),
					'status' => array(
					'type' => 'varchar',
					'length' => 80, 
					'not null' => TRUE,
					'default' => 'available',
					'description' => t('Current status of the book, whether available,lend or sold'),
					),
					'id' => array(
					'type' => 'varchar',
					'length' => 100, 
					'not null' => TRUE,
					'default' => 'mucu/1/13',
					'description' => t('Unique book identification code.'),
					),
					'selling_price' => array(
					'type' => 'varchar',
					'length' => 30, 
					'not null' => TRUE,
					'default' => 'N/A',
					'description' => t('Selling price of the book if its for sale.'),
					),
					'buying_price' => array(
					'type' => 'varchar',
					'length' => 30, 
					'not null' => TRUE,
					'default' => 'N/A',
					'description' => t('Purchase price of the book, if it was bought'),
					),										
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),
									
			),
			'primary key' => array('auto'),
			
			);
			// other non book materials
		$schema['library_materials'] = array(
			'description' => 'Stores information about non printable materials.',
			'fields' => array(
					'auto' => array(
					'type' => 'serial', // Required.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('Auto increment.'),
					),
					'name' => array(
					'type' => 'varchar',
					'length' => 100, 
					'not null' => TRUE,
					'description' => t('material name.'),
					),
					'id' => array(
					'type' => 'varchar',
					'length' => 100, 
					'not null' => TRUE,
					'description' => t('Unique id asigned.'),
					),
					'category' => array(
					'type' => 'varchar',
					'length' => 80, 
					'not null' => TRUE,
					'default' => 'lendable',
					'description' => t('Category of the book whish defines its lending rules or sales rules.'),
					),										
					'selling_price' => array(
					'type' => 'varchar',
					'length' => 30, 
					'not null' => TRUE,
					'default' => 'N/A',
					'description' => t('Selling price of the material if its for sale.'),
					),
					'buying_price' => array(
					'type' => 'varchar',
					'length' => 30, 
					'not null' => TRUE,
					'default' => 'N/A',
					'description' => t('Purchase price of the material, if it was bought'),
					),
					'status' => array(
					'type' => 'varchar',
					'length' => 80, 
					'not null' => TRUE,
					'default' => 'available',
					'description' => t('Current status of the book, whether available,lend or sold'),
					),
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),
			),
			'primary key' => array('auto'),
			);
			//lend table
			$schema['library_lend'] = array(
			'description' => 'Stores information about book lending',
			'fields' => array(
					'auto' => array(
					'type' => 'serial', // Required.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('Auto increment.'),
					),
					'admno' => array(
					'type' => 'varchar',
					'length' => 80, 
					'not null' => TRUE,
					'default' => 'mucu',
					'description' => t('Member admission number.'),
					),
					'id' => array( 
					'type' => 'varchar',
					'length' => 100, 
					'not null' => TRUE,
					'default' => 'mucu/1/13',
					'description' => t('Unique book identification code.'),
					),					
					'status' => array(
					'type' => 'varchar',
					'length' => 80, 
					'not null' => TRUE,
					'default' => 'mucu',
					'description' => t('Current state of the book, either lend or returned.'),
					),
					'lend_date' => array(
					'type' => 'varchar',
					'length' => 80, 
					'not null' => TRUE,
					'default' => 'mucu',
					'description' => t('Date the book was lend'),
					),
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),
										
			),
			'primary key' => array('auto'),
			
			);
			//transaction
			$schema['library_transaction'] = array(
			'description' => 'Stores information about all transactions.',
			'fields' => array(
					'auto' => array(
					'type' => 'serial', // Required.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('Auto inrement.'),
					),
					'id' => array(
					'type' => 'varchar',
					'length' => 100, 
					'not null' => TRUE,
					'default' => 'mucu/1/13',
					'description' => t('Unique transaction identification code.'),		
					),			
					'item' => array( 
					'type' => 'varchar',
					'length' => 100, 
					'not null' => TRUE,
					'description' => t('Item name.'),
					),
					'amount' => array(
					'type' => 'varchar',
					'length' => 50, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Amount .'),
					),
					'type' => array( 
					'type' => 'varchar',
					'length' => 50, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Transaction type, either sales or purchases'),
					),
					'transaction_date' => array( 
					'type' => 'varchar',
					'length' => 50, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Date the transaction was made.'),
					),
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),
			),
			'primary key' => array('auto'),
			);
			//category
			$schema['library_category'] = array(
			'description' => 'Stores about rules for various book categories.',
			'fields' => array(
					'auto' => array(
					'type' => 'serial', // Required.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('Auto inrement.'),
					),
					'name' => array(
					'type' => 'varchar',
					'length' => 100, 
					'not null' => TRUE,
					'description' => t('Category name.'),
					),
					'type' => array(
					'type' => 'varchar',
					'length' => 50, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Type of book whether salable or lendable .'),
					),
					'lend_days' => array(
					'type' => 'varchar',
					'length' => 50, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Maximum number of days one can lend books of this category'),
					),
					'lend_to' => array(
					'type' => 'varchar',
					'length' => 50, 
					'not null' => TRUE,
					'default' => '0',
					'description' => t('Restrict who should lend this books.'),
					),
					'date' => array(
					'type' => 'int', // Required.
					'unsigned' => TRUE, // Defaults to FALSE. Serial numbers are usually positive.
					'size' => 'big',
					'not null' => TRUE,
					'description' => t('When it was updated.'),
					),
			),
			'primary key' => array('auto'),
			);
		return $schema;
	}
function mucu_library_uninstall() {
  // Drop my tables.
  drupal_uninstall_schema('mucu_library');
}
					
