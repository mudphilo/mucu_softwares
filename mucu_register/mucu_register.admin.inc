<?php
/**
 * @file
 * Settings page callback file for the mucu_register module.
 */

/**
 * Menu callback;
 */

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function mucu_register_synchronize_settings($form, &$form_state) {
  $form = array();
  // only administrators can access this function

  // Generate the form - settings applying to all patterns first
  $form['synchronize_db'] = array(
    '#type' => 'fieldset',
    '#weight' => -30,
    '#title' => t('Database settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['synchronize_db']['mucu_dbhost'] = array(
    '#type' => 'textfield',
    '#title' => t('Database Hostname/IP'),
    '#default_value' => variable_get('mucu_dbhost', 'localhost'),
    '#description' => t("Database Hostname or IP, e.g.: localhost, 192.168.0.1, dbase.drupal.org."),
  );

  $form['synchronize_db']['mucu_dbengine'] = array(
    '#type' => 'textfield',
    '#title' => t('Database Engine'),
    '#default_value' => variable_get('mucu_dbengine', 'mysql'),
    '#description' => t("Database Engine, e.g.: mysql, mysqli, pgsql"),
  );

  $form['synchronize_db']['mucu_dbname'] = array(
    '#type' => 'textfield',
    '#title' => t('Database Name'),
    '#default_value' => variable_get('mucu_dbname', 'smsd'),
    '#description' => t("Database Name, e.g.: smsd, drpl_123, mydata."),
  );

  $form['synchronize_db']['mucu_dbuser'] = array(
    '#type' => 'textfield',
    '#title' => t('Database Username'),
    '#default_value' => variable_get('mucu_dbuser', ''),
    '#description' => t("Database Username, e.g.: root, user_123."),
  );

  $form['synchronize_db']['mucu_dbpass'] = array(
    '#type' => 'password',
    '#title' => t('Database Password'),
    '#default_value' => variable_get('mucu_dbpass', ''),
    '#description' => t("Database Password"),
  );

  return system_settings_form($form);
}

