<?php
function mucu_save_as_pdf_settings($form, &$form_state) {
  $form = array();
  // only administrators can access this function

  // Generate the form - settings applying to all patterns first
  $form['pdf'] = array(
    '#type' => 'fieldset',
    '#weight' => -30,
    '#title' => t('MUCU PDF settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
$form['bs'] = array(
    '#type' => 'fieldset',
    '#weight' => -10,
    '#title' => t('Bible study settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
   $form['pdf']['mucu_pdf_footer'] = array(
    '#type' => 'textarea',
    '#title' => t('Footer text'),
    '#default_value' => variable_get('mucu_pdf_footer', 'so i say walk in the Spirit and you will not gratify the desires of the flesh'),
    '#description' => t("Text that will appear as footer for PDF outputs"),
  );

  $form['pdf']['mucu_pdf_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Numbering text'),
    '#default_value' => variable_get('mucu_pdf_number', 'Valid MUCU document number: '),
    '#description' => t("Text that will appear alongside page numbers"),
  );
  $form['bs']['mucu_bs_title'] = array(
    '#type' => 'textfield',
    '#title' => t('BS group title'),
    '#default_value' =>variable_get('mucu_bs_title','MOI UNIVERSITY CHRISTIAN UNION BIBLE STUDY GROUPS'),
    '#description' => t("Title that will appear in bible study groups PDF"),
  );
   $form['bs']['groups_per_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of groups per page'),
    '#default_value' => variable_get('groups_per_page', '3'),
    '#description' => t("Select the number of groups that shall be printed in one page"),
  );
    $form['pdf']['mucu_main_org_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Short name for organization '),
    '#default_value' =>variable_get('mucu_main_account_name','MUCU'),
    '#description' => t("Short that will appear on document titles"),
  );
  return system_settings_form($form);
}

